Install
=======

Install from repository

	pip install -e git+https://guillaumepiot@bitbucket.org/guillaumepiot/cotidia-contact-form.git#egg=contact_form
	
Add to your INSTALLED_APPS setting:

	INSTALLED_APPS = (
		...
		'contact_form',
	)