Usage
=====

Include the template tags in your template

	{% load contact_form_tags %}
	
Include the contact form:

	{% contact_form %}