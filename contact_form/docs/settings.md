Settings
========

	# Who should receive the contact enquiry, must be a python list
	# Contact addresses are localised, 'en' being the default and mandatory local contact to provide
	CONTACT_EMAIL_DELIVER_TO = {'en':['info@example.com']}

	# The default subject line
	CONTACT_SUBJECT = _('Contact enquiry')
	
	# Add and display placeholder to the form fields
	CONTACT_FORM_SHOW_PLACEHOLDER = False
	
	CONTACT_FORM_PLACEHOLDER_NAME = 'Name'
	CONTACT_FORM_PLACEHOLDER_EMAIL = 'Email'
	CONTACT_FORM_PLACEHOLDER_TELEPHONE = 'Contact number'
	CONTACT_FORM_PLACEHOLDER_MESSAGE = 'Message'
	CONTACT_FORM_MESSAGE_ROWS = 3