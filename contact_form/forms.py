from django import forms
from django.utils.translation import ugettext as _

from contact_form import settings as contact_form_settings
from contact_form.models import Enquiry

class ContactForm(forms.ModelForm):
    name = forms.CharField(label=_('Name'), error_messages={ 'required':_('Please enter your name')})
    email = forms.EmailField(label=_('Email'), error_messages={'invalid': _('This email is not valid'), 'required':_('Please enter your email')}, widget=forms.TextInput())
    tel = forms.CharField(label=_('Telephone'), error_messages={ 'required':_('Please enter your telephone number')}, required=False)
    message = forms.CharField(label=_('Message'), error_messages={ 'required':_('Please enter your message')}, widget=forms.Textarea(attrs={'rows':'3', }))

    class Meta:
        model = Enquiry

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        if contact_form_settings.CONTACT_FORM_SHOW_PLACEHOLDER:
            self.fields['name'].widget.attrs={'placeholder':_(contact_form_settings.CONTACT_FORM_PLACEHOLDER_NAME)}
            self.fields['email'].widget.attrs={'placeholder':_(contact_form_settings.CONTACT_FORM_PLACEHOLDER_EMAIL)}
            self.fields['tel'].widget.attrs={'placeholder':_(contact_form_settings.CONTACT_FORM_PLACEHOLDER_TELEPHONE)}
            self.fields['message'].widget.attrs={'placeholder':_(contact_form_settings.CONTACT_FORM_PLACEHOLDER_MESSAGE), 'rows':contact_form_settings.CONTACT_FORM_MESSAGE_ROWS}
            #self.fields['message'].initial=_('Message')
        
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['email'].widget.attrs['class'] = 'form-control'
        self.fields['tel'].widget.attrs['class'] = 'form-control'
        self.fields['message'].widget.attrs['class'] = 'form-control'