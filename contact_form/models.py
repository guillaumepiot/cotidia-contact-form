from django.db import models
from django.utils.translation import ugettext as _

class Enquiry(models.Model):
    name = models.CharField(max_length=250)
    email = models.EmailField()
    tel = models.CharField(max_length=250, blank=True, null=True)
    message = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-date_created']
        verbose_name = _('Enquiry')
        verbose_name_plural = _('Enquiries')

