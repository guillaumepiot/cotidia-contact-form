from django.conf import settings
from django.utils.translation import ugettext as _

# Who should receive the contact enquiry, must be a python list
CONTACT_EMAIL_DELIVER_TO = getattr(settings, 'CONTACT_EMAIL_DELIVER_TO', {'en':['info@example.com']})

# The default subject line
CONTACT_SUBJECT = getattr(settings, 'CONTACT_SUBJECT', _('Contact enquiry'))

# Add and display placeholder to the form fields
CONTACT_FORM_SHOW_PLACEHOLDER = getattr(settings, 'CONTACT_FORM_SHOW_PLACEHOLDER', False)

CONTACT_FORM_PLACEHOLDER_NAME = getattr(settings, 'CONTACT_FORM_PLACEHOLDER_NAME', _('Name'))
CONTACT_FORM_PLACEHOLDER_EMAIL = getattr(settings, 'CONTACT_FORM_PLACEHOLDER_EMAIL', _('Email'))
CONTACT_FORM_PLACEHOLDER_TELEPHONE = getattr(settings, 'CONTACT_FORM_PLACEHOLDER_TELEPHONE', _('Contact number'))
CONTACT_FORM_PLACEHOLDER_MESSAGE = getattr(settings, 'CONTACT_FORM_PLACEHOLDER_MESSAGE', _('Message'))
CONTACT_FORM_MESSAGE_ROWS = getattr(settings, 'CONTACT_FORM_MESSAGE_ROWS', 3)
