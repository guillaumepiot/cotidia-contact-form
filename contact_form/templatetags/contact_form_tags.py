from django import forms
from django.core.mail import EmailMultiAlternatives
from django.utils import translation
from django.conf import settings
from django import template
from django.contrib import messages
register = template.Library()

from contact_form import settings as contact_form_settings
from contact_form.forms import ContactForm

@register.inclusion_tag('includes/_contact_form.html', takes_context=True)
def contact_form(context):
	request = context['request']

	initial = {}
	if request.GET.get('name', False):
		initial['name'] = request.GET['name']
	if request.GET.get('email', False):
		initial['email'] = request.GET['email']
	if request.GET.get('tel', False):
		initial['tel'] = request.GET['tel']
	if request.GET.get('message', False):
		initial['message'] = request.GET['message']

	form = ContactForm(initial=initial)
	success = False

	current_language = translation.get_language()

	recipients = contact_form_settings.CONTACT_EMAIL_DELIVER_TO.get(current_language, contact_form_settings.CONTACT_EMAIL_DELIVER_TO['en'])

	if request.method == 'POST':


		form = ContactForm(data=request.POST)
		if form.is_valid():
			form.save()
			success = True

			body = "Name: %s\n\nEmail: %s\n\nTel: %s\n\nMessage:\n\n%s" % (form.cleaned_data['name'], form.cleaned_data['email'], form.cleaned_data['tel'], form.cleaned_data['message'])

			msg = EmailMultiAlternatives(
						subject=contact_form_settings.CONTACT_SUBJECT,
						body=body,
						from_email=form.cleaned_data['email'],
						to=recipients
				)
			msg.send()

			messages.success(context['request'], 'Thank you for your message. We will get back to you shortly.')

	return {'form': form, 'success':success, }