��          �            h     i     y     �  	   �     �     �     �     �  "   �     �  	   �  <     V   E     �  k  �           3     F  	   K  	   U     _  	   d     n  #   �     �     �  <   �  l   �     a                                	                               
                 Contact enquiry Contact number Email Enquiries Enquiry Name Please enter your message Please enter your name Please enter your telephone number Send Telephone Thank you for your message. We will get back to you shortly. There has been an error in sending your message. Please complete all the fields below. This email is not valid Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-11-15 15:14+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Contatto richiesta Numero di contatto Mail Richieste Inchiesta Nome Messaggio Inserisci il tuo nome Inserisci il tuo numero di telefono Invia Telefono Grazie per il tuo messaggio. Ti risponderemo al più presto. C'è stato un errore nella spedizione del vostro messaggio. Si prega di compilare tutti i campi sottostanti. Questa email non è valido 