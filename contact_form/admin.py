from django.contrib import admin
from contact_form.models import Enquiry

class EnquiryAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'tel', 'message', 'date_created']

admin.site.register(Enquiry, EnquiryAdmin)